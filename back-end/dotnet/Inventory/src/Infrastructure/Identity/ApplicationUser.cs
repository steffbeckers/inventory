﻿using Microsoft.AspNetCore.Identity;

namespace Inventory.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
